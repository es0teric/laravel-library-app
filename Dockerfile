FROM php:7.2-apache
RUN apt-get update -y && apt-get install -y openssl zip unzip git
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN docker-php-ext-install pdo pdo_mysql mbstring json bcmath openssl tokenizer xml ctype
RUN ln -sf /dev/stdout /var/log/apache2/error.log
RUN a2enmod rewrite

EXPOSE 3306