<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
   /**
    * @var $fillable array columns allows to be accessed
    */
   protected $fillable = [
   	'isbn',
   	'book_title',
   	'author' 
	];

  public function checkoutLogs() {
   	return $this->hasMany( CheckoutLog::class );
  }

}
