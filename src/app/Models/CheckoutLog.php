<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CheckoutLog extends Model
{

	protected $table = 'checkout_logs';

	/**
	 * @var array $fillable
	 */
	protected $fillable = [
		'user_id',
		'book_id',
		'checkin_date',
		'due_date',
		'checkout_date'
	];

    public function book() {
    	return $this->belongsTo( Book::class );
    }

    public function user() {
    	return $this->belongsTo( User::class );
    }
}
