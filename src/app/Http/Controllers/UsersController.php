<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\CheckoutLog as Checkout;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class UsersController extends Controller
{
	/**
	 * Add user endpoint, must take email for a user to be added
	 * 
	 * @param object $request Request object which contains form data
	 * @return array $retval  Parsed data and messages concerning endpoint
	 */
    public function add( Request $request ) {

    	//lets set all of the request inputs that we need
    	$email = $request->input('email');

    	//lets get the user data that belogns to the email passed in
    	$user = User::where('email', $email)->first();

    	//lets set return variable
    	$retval = [];

    	//lets check if the user exists and is a librarian
    	if( !is_null( $user ) && $user->role === 'librarian' ) {

    		//lets set all the fields we need to create a user
    		$fields = [
    			'first_name' => $request->input('first_name'),
    			'last_name'	 => $request->input('last_name'),
    			'username'	 => $request->input('username'),
    			'password'	 => $request->input('password'),
    			'role'		 => $request->input('role')
    		];

    		//lets validate the input fields now
    		$validator = Validator::make( $fields, 
    			[
    				'first_name' => 'required|alpha|max:30',
    				'last_name'  => 'required|alpha|max:30',
    				'username'   => 'required|alpha_num|max:20',
    				'password'   => 'required|max:20',
    				'role'       => 'required|alpha|max:20'
    			]);

    		//lets validate the request fields first before adding the user
    		if( !$validator->fails() ) {

    			//lets create the user now
    			$created_user = User::create([
	    			'first_name' => $fields['first_name'],
	    			'last_name' => $fields['last_name'],
	    			'username' => $fields['username'],
	    			'password' => Hash::make( $fields['password'] ),
	    			'role' => $fields['role']
	    		]);

    			//check if the user was successfully added
	    		if( $created_user->exists ) {
	    			$retval['message']['user'] = 'User created successfully.';
	    		} else {
	    			$retval['message']['error']['user'] = 'User not created, please check logs and database.';
	    		}

    		} else {

    			//output validator messages
    			$retval['messages']['error'] = $validator->messages();

    		}

    	} else {
    		$retval['message']['error']['permissions'] = 'You do not have sufficient permissions to add a user.';
    	}

    	return response()->json( $retval );

    }

	/**
	 * Show endpoint, only provide username to display all users
     * if users role is librarian, it will display all users 
	 * 
	 * @param  object $request 
	 * @return array  $retval  returns error messages or requested user data 
	 */
    public function show( Request $request ) {

    	$username = $request->input('username');

        $user = User::where('username', $username)->first();

        $validator = Validator::make(['username' => $username], ['username' => 'required|alpha_num']);

        } else {

            $retval['message']['error'] = $validator->messages();
        
        }

    	return response()->json( $retval );

    }

    /**
     * Displays data of the individual username that was requested
     * 
     * @param  string   $username Username to be queried against
     * @return array    $retval  Return data of the username that was requested
     */
    public function display_user( Request $request, $username ) {

        //get user by username
        $user = User::where('username', $username)->first();

        //store return value
        $retval = [];

        //validate username
        $validator = Validator::make(['username' => $username], ['username' => 'required|alpha_num']);

        //lets check if the user exists before getting all user data
        if( !$validator->fails() && !is_null( $user ) && $user->exists ) {

            //lets get all checked out books by user
            if( count( $user->checkoutLogs->all() ) > 0 ) {

                //loop through all results of books that the user has checked out
                foreach( $user->checkoutLogs->all() as $checkout ) {

                    //assign currently checked out books to this array
                    $book[] = [
                        'book_title' => $checkout->book->book_title,
                        'author' => $checkout->book->author,
                        'checkout_date' => Carbon::parse( $checkout->checkout_date )->format('Y-m-d'),
                        'due_date' => Carbon::parse( $checkout->due_date )->format('Y-m-d')
                    ];

                }

                //output user data
                $retval['user'] = [
                    'first_name' => $user->first_name,
                    'last_name' => $user->last_name,
                    'username' => $user->username,
                    'role' => $user->role
                ];

                //output checked out books that user has
                $retval['user']['checked_out_books'] = $book;

            } else {

                $retval['message']['error']['user'] = 'This user does not have any checked out books.';

            }
            

        } else {
            $retval['message']['error'] = $validator->messages();
        }

        return response()->json( $retval );

    }

	/**
	 * Edit user endpoint
	 * 
	 * @param  object    $request Request object which contains form data
	 * @param  int       $user_id User ID to be searched for to edit
	 * @return array     $retval  Parsed data and messages concerning endpoint
	 */
    public function edit( Request $request, $user_id ) {

    	//grab username
    	$user_id = $request->input('user_id');

    	//get user by username
    	$user = User::where('id', $user_id)->first();

        //validate user_id
        $user_validator = Validator::make(['user_id' => $user_id], ['user_id' => 'required|numeric']);

    	//lets check if the user exists and is a librarian
    	if( !$user_validator->fails() && !is_null( $user ) && $user->role === 'librarian' ) {

    		//grab fields to use
    		$fields = [
    			'user_id' => $user_id,
    			'first_name' => $request->input('first_name'),
    			'last_name' => $request->input('last_name'),
    			'username' => $request->input('username'),
    			'password' => $request->input('password'),
    			'role' => $request->input('role')
    		];

	    	//lets set the validator rules
	    	$validator = Validate::make( $fields, 
	    		[
	    			'user_id'	 => 'required|numeric',
    				'first_name' => 'alpha',
    				'last_name'  => 'alpha',
    				'username'   => 'alpha_num',
    				'password'   => 'max:20',
    				'role'       => 'alpha'
    			]
	    	);

	    	//lets validate the user id to make sure 
	    	if( !$validator->fails() ) {
	    		
	    		//update user once validation passes
	    		$updated = $user->update([
	    			'first_name' => $fields['first_name'],
	    			'last_name' => $fields['last_name'],
	    			'username' => $fields['username'],
	    			'password' => Hash::make( $fields['password'] ),
	    			'role' => $fields['role']
	    		]);

	    		//check if user record from successfully updated
	    		if( $updated ) {
	    			$retval['message']['updated'] = 'User updated.';
	    		} else {
	    			$retval['message']['updated'] = 'User could not be updated, please check logs and database';
	    		}

	    	} else {

	    		//grab validator messages to output
	    		$retval['messages']['error'] = $validator->messages();
	    	}

	    } else {
	    	$retval['message']['error'] = $user_validator->messages();
	    }

	    return response()->json( $retval );

    }

	/**
	 * Delete user endpoint, MUST provide user_id
	 * 
	 * @param  object  $request Input request data
	 * @param  int     $user_id User ID to delete
	 * @return array   $retval  Messages or data to display to the user
	 */
    public function delete( Request $request, $user_id ) {

    	//grab username
    	$user_id = $request->input('user_id');

    	//get user by username
    	$user = User::where('id', $user_id)->first();

        //validate user_id
        $user_validator = Validator::make(['user_id' => $user_id], ['user_id' => 'required|numeric']);

    	//lets make sure the user exists and the user input is from a user that is in the database
    	if( !$user_validator->fails() && $user->role === 'librarian' ) {

    		//lets set the validator
    		$validate = Validator::make(['user_id' => $user_id], ['user_id' => 'require|numeric|max:1']);

    		//check validator and then delete user once it passes
    		if( !$validator->fails() ) {

    			$deleted_user = User::destroy( $user_id );

    			//check if the user successfully deleted
    			if( $deleted_user ) {

    				$retval['messages']['user'] = 'User successfully deleted.';

    			} else {

    				$retval['messages']['error']['user'] = 'User could not be deleted, please check logs and database.';

    			}

    		} else {

    			$retval['messages']['error'] = $validator->message();

    		}

    	} else {

    		$retval['messages']['error'] = $user_validator->messages();

    	}

    	return response()->json( $retval );

    }


}
