<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Book;
use App\Models\CheckoutLog as Checkout;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class BooksController extends Controller
{

	/**
	 * Add book endpoint to add book, only librarians can add books
	 * 'email' form data must be provided
	 * 
	 * @param object $request  Request object which contains form data
	 * @return array $retval   data to be displayed after book was successfully added
	 */
    public function add( Request $request ) {

    	//store email and sanitize
    	$email = $request->input('email');

    	//query users model to get users role from user_id
    	$user = User::where('email', $email)->first();

    	//set for return value
    	$retval = [];

    	//check if user role is librarian so we can create the appropriate books
    	if( !is_null( $user ) && $user->role === 'librarian' ) {

    		//define fields to check against in array
    		$fields = [
    			'isbn' => $request->input('isbn'),
    			'book_title' => $request->input('book_title'),
    			'author' => $request->input('author')
    		];

            $validator = Validator::make($fields, [
                'isbn' => 'required|numeric|max:13',
                'book_title' => 'required|alpha|max:20',
                'author' => 'required|alpha|max:20'
            ]);

            //lets check the required fields with the validator
            if( !$validator->fails() ) {

                //create new record in database
                $book = Book::create([
                    'isbn' => $isbn,
                    'book_title' => $book_title,
                    'author' => $author
                ]);

                //lets check if the record was correctly saved
                if( $book ) {
                    $retval['message']['update'] = 'The book was added successfully.';
                } else {
                    $retval['message']['update']['error'] = 'The book was not added successfully, please check the database and logs.';
                }

            } else {

                $retval['messages']['error'] = $validator->messages();

            }

    	} else {

    		//set error for user that does not have access to add a book
    		$retval['message']['error'] = 'This user does not have access to add a book, please ask site administrator for permissions.';

    	}

    	return response()->json( $retval );
    }

	/**
	 * Delete book endpoint, must provide book ID ONLY
	 * 
	 * @param  object $request Request object which contains delete form data
	 * @param  int 	  $book_id Book ID to check against and then delete
	 * @return array  $retval  Response array with messages
	 */
    public function delete( Request $request, $book_id ) {

    	//store retun value
    	$retval = [];

        $validator = Validate::make(['book_id' => $book_id], ['book_id' => 'required|numeric']);
        
        if( !$validator->fails() ) {
            
            //immediately delete book by book id
            $book = Book::destroy( $book_id );

        	//lets check if the book exists in the database to make sure it was successfully deleted
        	if( $book ) {
        		$retval['message']['book'] = 'Book successfully deleted.';
        	} else {
        		$retval['message']['book']['error'] = 'Error, book still exists please check logs and database';
        	}

        } else {
            
            $retval['message']['error'] = $validator->messages();

        }


    	return response()->json( $retval );

    }

	/**
	 * Show all books endpoint, MUST provide username
	 * 
     * @param  object $request   Request data
	 * @return array $retval Array of all books found in books table
	 */
    public function show( Request $request ) {

        //get all books that exist in the table
        $books = Book::all();

        $retval = [];

        //if table is empty, it will return null
        if( !is_null( $books ) || !empty( $books ) ) {

            //loop through all books and assign to $books_json to output to API
            foreach( $books as $book ) {

                //parse created datetime to format
                $created_at = Carbon::parse( $book->created_at );

                //retrieved books data
                $books_json[] = [
                    'isbn' => $book->isbn,
                    'book_title' => $book->book_title,
                    'author' => $book->author,
                    'date_added' => $created_at->format('F jS, Y')
                ];

            }

            $retval['data'] = $books_json;

        } else {

            $retval['messages']['error'] = 'There are no books in the table';

        }

    	return response()->json( $retval );

    }

	/**
	 * Endpoint to edit book by ID, only librarians can edit book information by ID
	 * 'email' form data must be provided
	 * 
	 * @param  object   $request Request object which contains form data
	 * @param  int 	    $book_id ID of book that is going to be edited
	 * @return array 	$retval  return value of JSON to be displayed
	 */
    public function edit( Request $request, $book_id ) {

    	//store email and sanitize
    	$email = $request->input('email');

    	//query users model to get users role from user_id
    	$user = User::where('email', $email)->first();

    	//set for return value
    	$retval = [];

    	//check if user role is librarian so we can edit the appropriate books
    	if( !is_null( $user ) && $user->role === 'librarian' ) {

    		//find book by id to edit
    		$book = Book::find( $book_id );

    		//set vars from post body define fields to check against in array
            $fields = [
                'isbn' => $request->input('isbn'),
                'book_title' => $request->input('book_title'),
                'author' => $request->input('author')
            ];

            $validator = Validator::make($fields, [
                'isbn' => 'numeric|max:13',
                'book_title' => 'required|alpha',
                'author' => 'required|alpha'
            ]);

            //check validator for fields if they were added correctly
            if( !$validator->fails() ) {

                //check if isbn number was provided if not empty, update if it is, keep original
                if( !empty( $fields['isbn'] ) ) {
                    $isbn = $fields['isbn'];
                } else {
                    $isbn = $book->isbn;
                }

                //update rows with new data
                $update_row = [
                    'isbn' => $isbn,
                    'book_title' => $fields['book_title'],
                    'author' => $fields['author']
                ];

                //update book row with information from post body
                $book->update( $update_row );

                //check if row saves and output message
                if( $book ) {
                    $retval['message']['update'] = 'The book was updated successfully.';
                } else {
                    $retval['message']['update']['error'] = 'The book was not updated successfully, please check the database and logs.';
                }

            } else {

                $retval['message']['error'] = $validator->messages();

            }

    		return response()->json( $retval );

    	}  else {

    		//set error for user that does not have access to add a book
    		$retval['message']['error'] = 'This user does not have access to edit a book, please ask site administrator for permissions.';
    		return response()->json( $retval );

    	}

    }

	/**
	 * Checkout book endpoint, must provide user email along with book_id to checkout
	 * 
	 * @param  object $request Request object which contains form data
	 * @param  int 	  $book_id Book ID to checkout 
	 * @return array  $retval  Data to display once a book has been successfully checked out
	 */
    public function checkout( Request $request, $book_id ) {

    	//store email request input 
    	$email = $request->input('email');

    	//get checkout boolean from input
    	$checkout = $request->input('checkout');

    	//define return value that contains messages or json data to output
    	$retval = [];

    	//lets get the user object to 
    	$user = User::where('email', $email)->first();

        $checkout_validator = Validator::make(['checkout' => $checkout], ['checkout' => 'required|boolean']);

    	//lets check if the user allowed to checkout a book
    	if( !$checkout_validator->fails() && 
            $checkout && 
            $user->exists && 
            !is_null( $user ) ) {

            $fields = [
                'book_id' => $book_id
            ];

            $validator = Validator::make($fields, ['book_id' => 'required|numeric']);

            //as long as the validator doesn't fail, checkout the book
            if( !$validator->fails() ) {

        		//Create book checkout
        		$checkout = Checkout::create([
    	    		'user_id' => $user->id,
    	    		'book_id' => $book_id,
    	    		'checkout_date' => Carbon::now()->format('Y-m-d'),
    	    		'due_date' => Carbon::now()->add('2', 'weeks')
    	    	]);

        		if( $checkout ) {
        			$retval['message']['checkout'] = 'Book successfully checked out.';
        		} else {
        			$retval['message']['checkout']['error'] = 'Book was not successfully checked out, please check logs and database.';
        		}

            } else {

                $retval['message']['error'] = $validator->messages();
            }

    	} else {
    		$retval['message']['error'] = $checkout_validator->messages();
    	}

    	return response()->json( $retval );
    }

	/**
	 * Checkin book endpoint, must provide user email and book_id for checkin to check in book 
	 * 
	 * @param  Object 	$request Request object which contains form data
	 * @param  int 		$book_id Book ID to checkout
	 * @return array 	$retval  Data to display once a book has been successfully checked in
	 */
    public function checkin( Request $request, $checkout_id ) {

    	//get checkin boolean
    	$checkin = $request->input('checkin');

    	//define return value that contains messages or json data to output
    	$retval = [];

    	//lets get the checkout data
    	$checkout = Checkout::where( 'id', $checkout_id )->first();

        //lets check the validator to make sure that user provided the correct value
        $validator = Validator::make(['checkin' => $checkin], ['checkin' => 'required|boolean']);

    	//lets check if the user can checkin a book because it exists
    	if( !$validator->fails() && $checkout->exists ) {

    		//get checkout record to modify for checkin
    		$checkin = Checkout::where( 'id', '=', $checkout_id )->first();

    		//update checkout record for checkin
    		$update = $checkin->update([ 
    			'checkin_date' => Carbon::now()
    		]);

            if( $update ) {
                $retval['message']['checkin'] = 'Book checked in successfully.';
            } else {
                $retval['message']['checkin']['error'] = 'Book not updated to be checked in, please check logs and database.';
            }

    	} else {
    		
            $retval['message']['checkin']['error'] = $validator->messages();

    	}

    	return response()->json( $retval );

    }

}
