<?php

use Illuminate\Database\Seeder;
use App\Models\CheckoutLog;
use Carbon\Carbon;

class CheckoutLogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //initiate faker
    	$faker = Faker\Factory::create();

        //loop though expected user_id int and insert into checkoutLog with associated
        //book_id int for logs
    	for( $i = 1; $i <= 7; $i++ ) {
            //define checkout_date
            $checkout_date = $faker->dateTimeBetween('now', $endDate = '2 weeks');
            $parsed_date = Carbon::parse( $checkout_date );;

	        CheckoutLog::create([
	        	'user_id' => random_int(1, 5),
	        	'book_id' => random_int(1, 8),
	        	'checkout_date' => $checkout_date,
	        	'due_date' => $parsed_date->add('2', 'weeks')
	        ]);
    	}
    }
}
