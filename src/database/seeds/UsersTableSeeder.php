<?php

use Illuminate\Database\Seeder;
use App\Models\User;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $faker = Faker\Factory::create();

       //create initial user for librarian (admin) role and insert into user table
       User::create([
          'id'          => 1,
       		'first_name'	=> $faker->firstName('male'),
       		'last_name' 	=> $faker->lastName('male'),
       		'email' 		  => $faker->email,
       		'username' 		=> $faker->userName,
       		'password' 		=> $faker->password,
       		'role' 			  => 'librarian'
       ]);

       //loop though expected amount of normal users to insert into users table
       for($i = 2; $i <= 5; $i++) {

       		User::create([
            'id' => $i,
	       		'first_name' 	=> $faker->firstName,
	       		'last_name' 	=> $faker->lastName,
	       		'email' 		=> $faker->email,
	       		'username' 		=> $faker->userName,
	       		'password' 		=> $faker->password,
	       		'role' 			=> 'user'
       		]);

       }
    }
}
