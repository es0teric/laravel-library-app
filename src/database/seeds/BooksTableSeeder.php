<?php

use Illuminate\Database\Seeder;
use App\Models\Book;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $faker = Faker\Factory::create();

       //loop though expected amount of books to insert into books table
       for($i = 1; $i <= 8; $i++) {
	      Book::create([
          'id' => $i,
	       	'isbn' => $faker->isbn13(),
	       	'book_title' => $faker->sentence(2),
	       	'author' => $faker->name()
	      ]);
	   }
    }
}
