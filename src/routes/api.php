<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {

	//Book CRUD
	Route::post('book/add/', 'BooksController@add');
	Route::post('book/edit/{book_id}', 'BooksController@edit');
	Route::delete('book/delete/{book_id}', 'BooksController@delete');
	Route::get('books', 'BooksController@show');
	Route::post('book/checkout/{book_id}', 'BooksController@checkout');
	Route::post('book/checkin/{checkout_id}', 'BooksController@checkin');
	
	//User CRUD
	Route::post('user/add', 'UsersController@add');
	Route::post('user/edit/{user_id}', 'UsersController@edit');
	Route::get('user/', 'UsersController@show');
	Route::post('user/{username}', 'UsersController@display_user');
	Route::delete('user/delete/{user_id}', 'UsersController@delete');


});