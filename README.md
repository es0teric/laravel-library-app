## Instructions on running Library App 

---

#### In the directory where you cloned this repository run:
`$ docker build .`

#### Then after that, run:
`$ docker-compose up`

This should automatically build the docker container, run laravel migrations and seed the database.

## API Endpoints

---



**Books Endpoint:**

> POST api/v1/book/add
> email (required), book_title (required), isbn, author (required)
>
> Adds book into database with title, author and ISBN number. Must provide email because the user must have role as librarian to add the book

> POST api/v1/book/checkin/{checkout_id}
> checkin (boolean, required), checkout_id (int, required)
>
> Checks in book based on checkout ID in the database, checkin boolean is required to confirm user wants to check in the book. Must use checkout endpoint to find out which book to check in.

> POST api/v1/book/checkout/{book_id}
> book_id (required), checkout (boolean, required)
>
> Checks out book based on book ID, must provide check out boolean to confirm user wants to checkout the book. Book ID is also required, must use books endpoint to get book ID.

> POST api/v1/book/edit/{book_id}
> Isbn (required), book_title (reqired), author (required)
>
> Edits book in database by book ID, user must provide all required fields to edit

>DELETE api/v1/book/delete/{book_id}
>book_id (required)
>
>Deletes book from database, must provide book ID to delete.

> GET api/v1/books
>
> Shows all books that are available in the database.

**User Endpoint**:

> GET api/v1/user 
>
> Displays all users if the user has a role of librarian

> POST api/v1/user/{username}
> username (required)
>
> Displays data of username provided

> POST api/v1/user/add
> email (required), first_name (required), last_name (required), username (required), password (required), role (required)
>
> Adds a new user to the database, needs email because user needs to be role librarian to add a new user

> POST api/v1/user/delete/{user_id}
> user_id (required)
>
> Deletes user from database

> POST api/v1/user/edit/{user_id}
> user_id (required), first_name, last_name, username, password, role
>
> Edits a user in the database with provided user ID